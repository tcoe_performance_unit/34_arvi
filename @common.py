from hyperload import *

MAIN_PAGE = [

    ["MAIN_PAGE_webshop-qa.arweb.pp.ciklum.com",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/'),
                        add_header('Pragma: no-cache',
                                   'Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Home page'),
                        TT(10),
                        # extract_from_body(r'/form_key/(.+?)/', 'FORM_KEY', order=1)
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

]

SET_LOCATION = [

    ["SET_LOCATION_setCustomerLocation",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/arvi_catalog/index/setCustomerLocation'),
                        body('cancel=true'),
                        add_header('Pragma: no-cache',
                                   'Accept: */*',
                                   'X-Requested-With: XMLHttpRequest',
                                   'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'),
                        TT(0.01),
                        ## >>>
     ],

]

ONEPAGE_PROGRESS_TOTALS = [

    ["ONEPAGE_PROGRESS_TOTALS_onepage/getProgressTotals/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/getProgressTotals/'),
                        add_header('Accept: application/json, text/javascript, */*; q=0.01',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('{"total_sidebar"'),
                        TT(0.01),
                        ## >>>  {"total_sidebar":"    <dd>\n        <table class=\"data-table \">\n                        <tr class=\"subtotals\">\n    <td style=\"\" class=\"a-right\" colspan=\"3\">\n        <span>Subtotal<\/span>...
     ],

]
