# coding=utf-8
from hyperload import *

script = [
    script_settings(
        cache_manager(AUTO),
        download_extra(AUTO),
        continue_on_error(OFF),
        cookie_manager(ON),
        gzip_compression(ON),
        tcp_connection_timeout(AUTO),
        user_agent(Chrome),
        # add_file('Register_new_users.csv', 'LOGIN,PASSWORD')
    ),

    ["REGISTER_PAGE_account/create/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/create/'),
                        add_header('Pragma: no-cache',
                                   'Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Create New Customer'),
                        TT(1),
                        extract_from_body('name="form_key" value="(.+?)"', 'FORM_KEY')
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    ["CREATE_NEW_USER_account/createpost/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/createpost/'),
                        upload_file_multipart({'param': [['success_url', ''],
                                                         ['error_url', ''],
                                                         ['form_key', '${FORM_KEY}'],
                                                         ['group_id', '1'],
                                                         ['company', ''],
                                                         ['spoken_language', '16'],
                                                         ['firstname', 'First'],
                                                         ['lastname', 'Last'],
                                                         ['email', '${__UUID}@arvi-test.com'],
                                                         ['password', '123456Test'],
                                                         ['confirmation', '123456Test']]}),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/create/'),
                        verify_text('My Account'),
                        TT(1),
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/index/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],

    ["ADDRESSES_PAGE_arvi_customer/address/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/address/'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/index/'),
                        verify_text('Add New Address'),
                        TT(1),
                        python_assertion('''
import json
import re
from random import choice

content = @get_response_body()

form_key = re.search(r'name="form_key" type="hidden" value="(.+?)"', content).group(1)
finder = re.search(r'\\'region_id\\', ({"config":.+})', content).group(1)
random_region = choice(json.loads(finder)['CH'].keys())
@set_var('FORM_KEY', form_key)
@set_var('REGION_ID', random_region)
''')
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/address/new/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],

    ["ADD_NEW_ADDRESS_ustomer/address/formPost/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/address/formPost/'),
                        upload_file_multipart({'param': [['form_key', '${FORM_KEY}'],
                                                         ['success_url', ''],
                                                         ['error_url', ''],
                                                         ['firstname', 'First'],
                                                         ['lastname', 'Last'],
                                                         ['company', ''],
                                                         ['telephone', '+41661234567'],
                                                         ['fax', ''],
                                                         ['address_name', ''],
                                                         ['country_id', 'CH'],
                                                         ['region_id', '${REGION_ID}'],
                                                         ['region', ''],
                                                         ['city', 'City'],
                                                         ['postcode', '123456'],
                                                         ['street[]', 'Street'],
                                                         ['street[]', ''],
                                                         ['default_billing', '1'],
                                                         ['default_shipping', '1']]}),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/address/new/'),
                        verify_text('Address Book'),
                        TT(1),
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/address/index/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],



]

if __name__ == '__main__':
    DSLConverter.compile_DSL_to_Jmeter_test_plan(mode='single_script')
    # DSLConverter.compile_DSL_to_Jmeter_test_plan()
