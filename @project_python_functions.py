def decode_string(origin):
    import urllib
    import re

    set_of_finds = set(re.findall(r'(&#x.{1,2};)', origin))
    parsed_dict = {key: unichr(eval(r'0x' + key[3:-1])) for key in set_of_finds}

    for key, val in parsed_dict.items():
        origin = origin.replace(key, val)

    origin = urllib.quote(origin, safe='').replace('%0A', '%0D%0A')

    return origin

