# coding=utf-8
from hyperload import *

script = [
    script_settings(
        cache_manager(AUTO),
        download_extra(AUTO),
        continue_on_error(OFF),
        cookie_manager(ON),
        gzip_compression(ON),
        tcp_connection_timeout(AUTO),
        user_agent(Chrome),
        add_file('search_words.csv', 'QUERY')
    ),

    # '@common.MAIN_PAGE',
    #
    # '@common.SET_LOCATION',

    python_sampler('Word divider', '''
import time

time.sleep(10)

word = @get_var('QUERY')
char_list = [word[:2]]
for i in range(len(word) - 2):
    char_list.append(word[:3 + i])

for ind, item in enumerate(char_list[:4], start=1):
    @set_var('SEARCHER_' + str(ind), item)
'''),

    foreach_controller('SEARCHER'),

        ["ELASTIC_SEARCH_autocomplete.php",
                            GET('https://webshop-qa.arweb.pp.ciklum.com/autocomplete.php?store=arvi_storeview_ch&fallback_url='
                                'https://webshop-qa.arweb.pp.ciklum.com/catalogsearch/ajax/suggest/&q=${SEARCHER}'),
                            add_header('Host: webshop-qa.arweb.pp.ciklum.com',
                                       'Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                       'X-Prototype-Version: 1.7.3',
                                       'X-Requested-With: XMLHttpRequest',
                                       'Accept-Encoding: gzip, deflate, br'),
                            verify_text('elasticsearch'),
                            TT(0.5),
         ],

    end_foreach,

    ["SEARCH_CATALOG_catalogsearch/result/?q={QUERY}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/catalogsearch/result/?q=${QUERY}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Search results'),
                        TT(5),
     ],

]

if __name__ == '__main__':
    DSLConverter.compile_DSL_to_Jmeter_test_plan(mode='single_script')
    # DSLConverter.compile_DSL_to_Jmeter_test_plan()
