# coding=utf-8
from hyperload import *

script = [
    script_settings(
        cache_manager(AUTO),
        download_extra(AUTO),
        continue_on_error(OFF),
        cookie_manager(ON),
        gzip_compression(ON),
        tcp_connection_timeout(AUTO),
        user_agent(Chrome),
    ),

    '@common.MAIN_PAGE',

    '@common.SET_LOCATION',

    ["ALL_WINES_wines",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/wines/'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('All wines'),
                        TT(5),
                        extract_from_body('wines/(((?!producer).)*?__.*?)"', 'FIRST_FILTER', order=random),
                        python_assertion('''
from random import randint
import re

body = @get_response_body()
searcher = re.search(r'wines/\?p=(\d+?)">\\1</a>', body)

max_page = int(searcher.group(1))

random_page = str(randint(2, max_page))
@set_var('RANDOM_PAGE', random_page)
''')
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.SET_LOCATION',

    ["OTHER_PAGE_wines/?p={RANDOM_PAGE}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/wines/?p=${RANDOM_PAGE}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/javascript, text/html, application/xml, text/xml, */*'),
                        verify_text('Page #'),
                        TT(5),
                        python_assertion('''
import re
from random import sample

data = @get_response_body()
list_of_ids = re.findall(r'/([a-z0-9\-]+?)" title=".+?" class="product-image">', data)
if len(list_of_ids) >= 7:
    sample_ids = sample(list_of_ids, 7)
else:
    sample_ids = list_of_ids

for ind, item in enumerate(sample_ids, start=1):
    @set_var('PRODUCT_ID_' + str(ind), item)
''')
                        ## >>>  {"title":"Page #2. \n                    France Red All wines. Buy France Red All wines in best wine shop. Prices for one bottle. | ARVI SA","content":"<div class=\"category-products _wine\">\n    <di...
     ],

    foreach_controller('PRODUCT_ID'),

    ["SELECT_ITEM_{PRODUCT_ID}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/${PRODUCT_ID}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        TT(5),
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.SET_LOCATION',

    end_foreach,

    ["RANDOM_FILTER_wines/{FIRST_FILTER}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/wines/${FIRST_FILTER}'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('{"title"'),
                        TT(5),
                        # extract_from_body(r'__(\w+?__\w+?)\\"', 'SECOND_FILTER', no_check, order=random),
                        ## >>>  {"title":"France All wines. Buy France All wines in best wine shop. Prices for one bottle. | ARVI SA","content":"<div class=\"category-products _wine\">\n    <div class=\"toolbar _grid\">\n    <div cl...
     ],


    # if_controller('"${SECOND_FILTER}" != "@@@SECOND_FILTER_undefined"'),
    #
    #     ["FILTER_BY_COLOR_wines/{FIRST_FILTER}__{SECOND_FILTER}",
    #                         GET('https://webshop-qa.arweb.pp.ciklum.com/wines/${FIRST_FILTER}__${SECOND_FILTER}'),
    #                         add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
    #                                    'X-Prototype-Version: 1.7.3',
    #                                    'X-Requested-With: XMLHttpRequest'),
    #                         verify_text('{"title"'),
    #                         TT(5),
    #                         extract_from_body(r'\\/([a-z0-9\-]+?)\\" title=\\".+?\\" class=\\"product-image\\">',
    #                                           'PRODUCT_ID', no_check, order=random)
    #                         ## >>>  {"title":"France Red All wines. Buy France Red All wines in best wine shop. Prices for one bottle. | ARVI SA","content":"<div class=\"category-products _wine\">\n    <div class=\"toolbar _grid\">\n   ...
    #      ],
    #
    # end_if,

]

if __name__ == '__main__':
    # DSLConverter.compile_DSL_to_Jmeter_test_plan(mode='single_script')
    DSLConverter.compile_DSL_to_Jmeter_test_plan(mode='scenario', scenario_name='Smoke_test')
