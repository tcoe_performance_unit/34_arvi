# coding=utf-8
from hyperload import *

script = [
    script_settings(
        cache_manager(AUTO),
        download_extra(AUTO),
        continue_on_error(OFF),
        cookie_manager(ON),
        gzip_compression(ON),
        tcp_connection_timeout(AUTO),
        user_agent(Chrome),
        # add_file('Users.csv', 'LOGIN,PASSWORD'),
        add_file('Users.csv', 'USER,PASS')
    ),

    python_sampler('User_setter', '''
from random import randint

if not @get_var('LOGIN'):
    @set_var('LOGIN', @get_var('USER'))
if not @get_var('PASSWORD'):
    @set_var('PASSWORD', @get_var('PASS')) 

if randint(1, 100) < 75:
    @set_var('NEW_USER', 'true')
else:
    @set_var('NEW_USER', 'false')
'''),

    '@common.MAIN_PAGE',

    '@common.SET_LOCATION',

    ["ALL_WINES_wines",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/wines/en_primeur__0'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('All wines'),
                        TT(5),
                        # extract_from_body('wines/(((?!producer).)*?__.*?)"', 'FIRST_FILTER', order=random),
                        python_assertion('''
from random import randint
import re

body = @get_response_body()
searcher = re.search(r'en_primeur__0\?p=(\d+?)">\\1</a>', body)

max_page = int(searcher.group(1))

random_page = str(randint(2, max_page))
@set_var('RANDOM_PAGE', random_page)
''')
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.SET_LOCATION',

    ["OTHER_PAGE_wines/?p={RANDOM_PAGE}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/wines/en_primeur__0?p=${RANDOM_PAGE}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Page #'),
                        TT(5),
                        extract_from_body(r'/([a-z0-9\-]+?)" title=".+?" class="product-image">',
                                          'PRODUCT_ID', no_check, order=random)
                        ## >>>  {"title":"Page #2. \n                    France Red All wines. Buy France Red All wines in best wine shop. Prices for one bottle. | ARVI SA","content":"<div class=\"category-products _wine\">\n    <di...
     ],

    ["SELECT_ITEM_{PRODUCT_ID}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/${PRODUCT_ID}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        # verify_text('Hermitage wine'),
                        TT(5),
                        extract_from_body(r'"product_id":(\d+?)\}', 'PRODUCT_ID', order=random),
                        extract_from_body(r'uenc/(.+?)/', 'REFERRER', order=1),
                        extract_from_body(r'title="Add bottle" data-url="https://.*?/(.+?)" class="button', 'ADD_BOTTLE_LINK', no_check),
                        extract_from_body(r'data-url="https://.+?(checkout.+?\?cases.+?)" type="button" class="button',
                                          'ADD_CASE_LINK', no_check)
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.SET_LOCATION',

    ["SELECT_SIZE_OR_VINTAGE_getProductInfoAjax/id/{PRODUCT_ID}",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/arvi_catalog/product/getProductInfoAjax/id/${PRODUCT_ID}'),
                        add_header('Accept: */*',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('{"sku"'),
                        TT(5),
                        ## >>>  {"sku":"2796-35116","qty_available":"5","country":"France","region":"Bordeaux","denomination":"Pauillac","producer":"Ch\u00e2teau Lafite Rothschild","color":"Red","classification":"1er Grand Cru Class...
     ],

    if_controller('"${ADD_BOTTLE_LINK}" != "@@@ADD_BOTTLE_LINK_undefined"'),

        ["ADD_BOTTLE_TO_CART_{ADD_BOTTLE_LINK}",
                            GET('https://webshop-qa.arweb.pp.ciklum.com/${ADD_BOTTLE_LINK}',
                                'isArviAjax=true',
                                'referer=${REFERRER}',
                                'blocks%5B%5D=minicart_head',
                                'blocks%5B%5D=ajax-cart',
                                'blocks%5B%5D=minicart_head_mobile',
                                'blocks%5B%5D=top-links',
                                'blocks%5B%5D=checkout-cart'),
                            add_header('Accept: */*',
                                       'X-Requested-With: XMLHttpRequest'),
                            verify_text('{"code"'),
                            TT(5),
                            python_assertion("@set_var('ADD_CASE', 'true')"),
                            # add_variable('ADD_CASE=true')
                            ## >>>  [{"code":"minicart_head","html":"<div class=\"minicart_head\">\n    <a href=\"https:\/\/webshop-qa.arweb.pp.ciklum.com\/checkout\/cart\/\" data-target-element=\"#header-cart\"\n       class=\"skip-lin...
         ],

    end_if,

    if_controller('"${ADD_CASE_LINK}" != "@@@ADD_CASE_LINK_undefined" && !("${ADD_CASE}" == "true")'),

        ["ADD_CASE_TO_CART_{ADD_TO_CART_LINK}",
                            GET('https://webshop-qa.arweb.pp.ciklum.com/${ADD_CASE_LINK}&',
                                # 'cases%5B1%5D=1',
                                'isArviAjax=true',
                                'referer=${REFERRER}',
                                'blocks%5B%5D=minicart_head',
                                'blocks%5B%5D=ajax-cart',
                                'blocks%5B%5D=minicart_head_mobile',
                                'blocks%5B%5D=top-links',
                                'blocks%5B%5D=checkout-cart'),
                            add_header('Accept: */*',
                                       'X-Requested-With: XMLHttpRequest'),
                            verify_text('{"code"'),
                            TT(5),
                            ## >>>  [{"code":"minicart_head","html":"<div class=\"minicart_head\">\n    <a href=\"https:\/\/webshop-qa.arweb.pp.ciklum.com\/checkout\/cart\/\" data-target-element=\"#header-cart\"\n       class=\"skip-lin...
         ],

    end_if,

    ["GO_TO_CART_checkout/cart/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/cart/'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Shopping Cart'),
                        TT(5),
                        extract_from_body(r'/form_key/(.+?)/', 'FORM_KEY', order=1)
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.SET_LOCATION',

    ["PROCEED_TO_CHECKOUT_checkout/onepage/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Checkout'),
                        TT(5),
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.ONEPAGE_PROGRESS_TOTALS',

    '@common.SET_LOCATION',

    if_controller('"${NEW_USER}" == "true"'),

    ["CREATE_NEW_USER_account/createpost/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/createpost/'),
                        upload_file_multipart({'param': [['success_url', ''],
                                                         ['error_url', ''],
                                                         ['form_key', '${FORM_KEY}'],
                                                         ['group_id', '1'],
                                                         ['company', ''],
                                                         ['spoken_language', '16'],
                                                         ['firstname', 'First'],
                                                         ['lastname', 'Last'],
                                                         ['email', '${__UUID}@arvi-test.com'],
                                                         ['password', '123456Test'],
                                                         ['confirmation', '123456Test']]}),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/'),
                        verify_text('My Account'),
                        TT(10),
                        extract_from_body(r'name="form_key" type="hidden" value="(.+?)"', 'FORM_KEY'),
                        extract_from_body(r'name="billing\[address_id\]" value="(\d+)"', 'BILLING_ID'),
                        python_assertion('''
import json
import re
from random import choice

content = @get_response_body()

finder = re.search(r'(\\{"config":.+\\})', content).group(1)
random_region = choice(json.loads(finder)['CH'].keys())
@set_var('FIRST_NAME', 'First')
@set_var('LAST_NAME', 'Last')
@set_var('REGION_ID', random_region)
@set_var('COUNTRY_ID', 'CH')
@set_var('CITY', 'City')
@set_var('POSTCODE', '123456')
@set_var('STREET', 'Street')
@set_var('PHONE', '+41661234567')
@set_var('ADDRESS_ID', '')
''')
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/index/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],

    end_if,

    if_controller('"${NEW_USER}" == "false"'),

    ["LOGIN_account/loginPost/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/loginPost/'),
                        body('form_key=${FORM_KEY}',
                             'login%5Busername%5D=${__urlencode(${LOGIN})}',
                             'login%5Bpassword%5D=${PASSWORD}',
                             'context=checkout'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Checkout'),
                        TT(10),
                        extract_from_body(r'billing.newAddress\(!this.value\)"><option value="(\d+?)"', 'ADDRESS_ID'),
                        extract_from_body(r'name="billing\[address_id\]" value="(\d+)"', 'BILLING_ID'),
                        extract_from_body(r'name="billing\[firstname\]"\n.*?value="(.+?)"', 'FIRST_NAME'),
                        extract_from_body(r'name="billing\[lastname\]"\n.*?value="(.+?)"', 'LAST_NAME'),
                        extract_from_body(r'<option value="(.{2})"selected="selected">', 'COUNTRY_ID'),
                        extract_from_body(r'name="billing\[city\]" value="(.+?)"', 'CITY'),
                        extract_from_body(r'id="billing:postcode" value="(.+?)"', 'POSTCODE'),
                        extract_from_body(r'id="billing:street1" value="(.+?)"', 'STREET'),
                        extract_from_body(r'name="shipping\[telephone\]" value="(.+?)"', 'PHONE'),
                        extract_from_body(r'name="form_key" type="hidden" value="(.+?)"', 'FORM_KEY')
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/index/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],

    end_if,

    '@common.ONEPAGE_PROGRESS_TOTALS',

    '@common.SET_LOCATION',

    ["SELECT_BILLING_ADDRESS_onepage/saveBilling/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/saveBilling/'),
                        body('form_key=${FORM_KEY}',
                             'billing_address_id=${ADDRESS_ID}',
                             'billing%5Baddress_id%5D=${BILLING_ID}',
                             'billing%5Baddress_name%5D=',
                             'billing%5Bfirstname%5D=${FIRST_NAME}',
                             'billing%5Blastname%5D=${LAST_NAME}',
                             'billing%5Bcompany%5D=',
                             'billing%5Bcountry_id%5D=${COUNTRY_ID}',
                             'billing%5Bregion_id%5D=${REGION_ID}',
                             'billing%5Bregion%5D=',
                             'billing%5Bcity%5D=${CITY}',
                             'billing%5Bpostcode%5D=${POSTCODE}',
                             'billing%5Bstreet%5D%5B%5D=${STREET}',
                             'billing%5Bstreet%5D%5B%5D=',
                             'billing%5Btelephone%5D=${__urlencode(${PHONE})}',
                             'billing%5Bfax%5D=',
                             'billing%5Bsave_in_address_book%5D=1',
                             'billing%5Buse_for_shipping%5D=0'),
                        add_header('Content-type: application/x-www-form-urlencoded; charset=UTF-8',
                                   'Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('goto_section'),
                        TT(5),
                        ## >>>  {"goto_section":"delivery"}
     ],

    # end_if,

    ["GET_ADDITIONAL_onepage/getAdditional/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/getAdditional/'),
                        add_header('Content-type: application/x-www-form-urlencoded; charset=UTF-8',
                                   'Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        TT(0.01),
                        ## >>>
     ],

    ["SET_CURRENT_PROGRESS_onepage/progress/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/progress/?prevStep=billing'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('changeSection'),
                        TT(0.01),
                        ## >>>  <dt class="complete">\n       <a href="#billing"\n                onclick="checkout.changeSection('opc-billing'); return false;">Billing Address</a>\n\n    </dt>
     ],

    ["SET_CURRENT_PROGRESS_onepage/progress/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/progress/'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        TT(0.01),
                        ## >>>
     ],

    ["SET_CURRENT_PROGRESS_onepage/progress/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/progress/?prevStep=delivery'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('Delivery'),
                        TT(0.01),
                        ## >>>  <dt class="simple">\n    Delivery Methods</dt>
     ],

    '@common.ONEPAGE_PROGRESS_TOTALS',

    '@common.ONEPAGE_PROGRESS_TOTALS',

    '@common.ONEPAGE_PROGRESS_TOTALS',

    ["SELECT_DELIVERY_TYPE_onepage/saveDelivery",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/arvi_checkout/onepage/saveDelivery'),
                        body('delivery_data=form_key%3D${FORM_KEY}'
                             '%26delivery%255Bdelivery_method%255D%3Dtoyou'
                             '%26shipping_address_id%3D${ADDRESS_ID}'
                             '%26shipping%255Bsame_as_billing%255D%3D1'
                             '%26shipping%255Baddress_id%255D%3D${BILLING_ID}'
                             '%26shipping%255Baddress_name%255D%3D'
                             '%26shipping%255Bfirstname%255D%3D${FIRST_NAME}'
                             '%26shipping%255Blastname%255D%3D${LAST_NAME}'
                             '%26shipping%255Bcompany%255D%3D'
                             '%26shipping%255Bcountry_id%255D%3D${COUNTRY_ID}'
                             '%26shipping%255Bregion_id%255D%3D${REGION_ID}'
                             '%26shipping%255Bregion%255D%3D'
                             '%26shipping%255Bcity%255D%3D${CITY}'
                             '%26shipping%255Bpostcode%255D%3D${POSTCODE}'
                             '%26shipping%255Bstreet%255D%255B%255D%3D${STREET}'
                             '%26shipping%255Bstreet%255D%255B%255D%3D'
                             '%26shipping%255Btelephone%255D%3D${__urlencode(${__urlencode(${PHONE})})}'
                             '%26shipping%255Bfax%255D%3D'
                             '%26shipping%255Bsave_in_address_book%255D%3D1'
                             '%26delivery%255Bstore_pickup%255D%3Denoteca'
                             '%26shipping%255Bdelivery_date_visible%255D%3D'
                             '%26shipping%255Bdelivery_date%255D%3D'),
                        add_header('Accept: application/json, text/javascript, */*; q=0.01',
                                   'X-Requested-With: XMLHttpRequest',
                                   'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'),
                        verify_text('{"allow_sections"'),
                        TT(5),
                        python_assertion('''
import re
from random import choice

content = @get_response_body_as_json()['update_section']['html']
finder = re.search(r'var shippingCodePrice = ({.+?})', content).group(1)
shippings = [key for key, val in eval(finder).items() if val > 0]
if shippings:
    random_shipping = choice(shippings)
else:
    random_shipping = 'arvi_tbcshipping_tbc'
@set_var('SHIPPING_METHOD', random_shipping)
'''),
                        # extract_from_body('value="(.+?)" .+? name="shipping_method"', 'SHIPPING_METHOD'),
                        # extract_from_body(r'id=\\"s_method_arvi_tbcshipping_tbc\\"\\n\s+value=\\"(.+?)\\"', 'SHIPPING_METHOD')
                        ## >>>  {"allow_sections":["shipping_method"],"goto_section":"shipping_method","update_section":{"name":"shipping-method","html":"    \n\n<ul class=\"messages\"><li class=\"success-msg\"><ul><li>Goods will be...
     ],

    ["SET_CURRENT_PROGRESS_onepage/progress/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/progress/?prevStep=delivery'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('changeSection'),
                        TT(0.01),
                        ## >>>  <dt class="complete">\n        <a href="#delivery"\n        onclick="checkout.changeSection('opc-delivery'); return false;">Delivery Methods        </a>\n</dt>
     ],

    '@common.ONEPAGE_PROGRESS_TOTALS',

    ["SELECT_SHIPPING_METHOD_onepage/saveShippingMethod/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/DatatransCw/onepage/saveShippingMethod/'),
                        body('form_key=${FORM_KEY}',
                             'shipping_method=arvi_tbcshipping_tbc'),  # ${SHIPPING_METHOD}
                        add_header('Content-type: application/x-www-form-urlencoded; charset=UTF-8',
                                   'Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('{"goto_section"'),
                        TT(5),
                        extract_from_body(r'\\/saveOrder\\/form_key\\/(.+?)\\/', 'FORM_KEY')
                        ## >>>  {"goto_section":"payment","update_section":{"name":"payment-method","html":"\n            <dt id=\"dt_method_banktransfer\" class=\"radio radio-primary\">\n        <label for=\"p_method_banktransfer\"...
     ],

    ["SET_CURRENT_PROGRESS_onepage/progress/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/progress/?prevStep=shipping_method'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('shipping_method'),
                        TT(0.01),
                        ## >>>  <dt class="complete">\n    <a  href="#shipping_method"\n        onclick="checkout.changeSection('opc-shipping_method'); return false;">Shipping Method</a>\n</dt>
     ],

    ["SET_CURRENT_PROGRESS_onepage/progress/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/progress/'),
                        add_header('Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        TT(0.01),
                        ## >>>
     ],

    '@common.ONEPAGE_PROGRESS_TOTALS',

    '@common.ONEPAGE_PROGRESS_TOTALS',

    ["SAVE_ORDER_form_key/{FORM_KEY}/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/saveOrder/form_key/${FORM_KEY}/'),
                        body('form_key=${FORM_KEY}',
                             'payment%5Bmethod%5D=datatranscw_creditcard'),
                        add_header('Content-type: application/x-www-form-urlencoded; charset=UTF-8',
                                   'Origin: https://webshop-qa.arweb.pp.ciklum.com',
                                   'Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/',
                                   'Accept-Encoding: gzip, deflate, br',
                                   'Accept-Language: en-US,en;q=0.9,uk;q=0.8,ru;q=0.7',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('{"success":true'),
                        TT(5),
                        ## >>>  {"success":true,"error":false,"redirect":"https:\/\/webshop-qa.arweb.pp.ciklum.com\/DatatransCw\/process\/dummy\/"}
     ],

    transaction_controller('Place Order WS -> DT'),

    ["PLACE_ORDER_process/ajax/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/DatatransCw/process/ajax/'),
                        add_header('Content-type: application/x-www-form-urlencoded; charset=UTF-8',
                                   'Accept: text/javascript, text/html, application/xml, text/xml, */*',
                                   'X-Prototype-Version: 1.7.3',
                                   'X-Requested-With: XMLHttpRequest',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/',
                                   'Origin: https://webshop-qa.arweb.pp.ciklum.com',
                                   'Accept-Encoding: gzip, deflate, br',
                                   'Accept-Language: en-US,en;q=0.9,uk;q=0.8,ru;q=0.7'),
                        verify_text('{error: "no"'),
                        TT(0.01),
                        extract_from_body(r'"data-upp-module-version", "(.+?)"', 'MODULE_VERSION'),
                        extract_from_body(r'"data-merchant-id", "(\d+?)"', 'MERCHANT_ID'),
                        extract_from_body(r'"data-amount", "(\d+?)"', 'AMOUNT'),
                        extract_from_body(r'"data-currency", "(.+?)"', 'CURRENCY'),
                        extract_from_body(r'"data-refno", "(.+?)"', 'REFNO'),
                        extract_from_body(r'"data-success-url", "(.+?)"', 'SUCCESS_URL'),
                        extract_from_body(r'"data-error-url", "(.+?)"', 'ERROR_URL'),
                        extract_from_body(r'"data-cancel-url", "(.+?)"', 'CANCEL_URL'),
                        extract_from_body(r'"data-cw-data-trans-id", "(\d+?)"', 'TRANS_ID'),
                        extract_from_body(r'"data-theme", "(.+?)"', 'THEME'),
                        extract_from_body(r'"data-sign", "(\d+?)"', 'SIGN'),
                        extract_from_body(r'"data-upp-customer-country", "(.+?)"', 'COUNTRY_ID'),
                        extract_from_body(r'/js/datatrans-(.+?)\.js"', 'VERSION')
                        ## >>>  {error: "no",javascriptUrl: "https://pilot.datatrans.biz/upp/payment/js/datatrans-1.0.2.js",callbackFunction:  function(formFieldValues) { datatransFormFields = formFieldValues; if(typeof window.jQuer...
     ],

    ["PLACE_ORDER_DT_jsp/upStart.jsp",
                        POST('https://pilot.datatrans.biz/upp/jsp/upStart.jsp'),
                        body('uppModuleName=Customweb+Magento',
                             'uppModuleVersion=${MODULE_VERSION}',
                             'merchantId=${MERCHANT_ID}',
                             'amount=${AMOUNT}',
                             'currency=${CURRENCY}',
                             'refno=${REFNO}',
                             'successUrl=${__urlencode(${SUCCESS_URL})}',
                             'errorUrl=${__urlencode(${ERROR_URL})}',
                             'cancelUrl=${__urlencode(${CANCEL_URL})}',
                             'useAlias=yes',
                             'uppReturnMaskedCC=yes',
                             'language=en',
                             'reqtype=NOA',
                             'testOnly=yes',
                             'uppCustomerName=${FIRST_NAME}+${LAST_NAME}',
                             'uppCustomerFirstName=${FIRST_NAME}',
                             'uppCustomerLastName=${LAST_NAME}',
                             'uppCustomerStreet=${STREET}',
                             'uppCustomerCity=${CITY}',
                             'uppCustomerCountry=${COUNTRY_ID}',
                             'uppCustomerZipCode=${POSTCODE}',
                             'uppCustomerEmail=${__urlencode(${LOGIN})}',
                             'uppCustomerDetails=yes',
                             'paymentmethod=VIS',
                             'paymentmethod=ECA',
                             'paymentmethod=AMX',
                             'paymentmethod=DIN',
                             'paymentmethod=DNK',
                             'paymentmethod=JCB',
                             'paymentmethod=JEL',
                             'paymentmethod=MAU',
                             'paymentmethod=MYO',
                             'cwDataTransId=${TRANS_ID}',
                             'theme=${THEME}',
                             'sign=${SIGN}',
                             'version=${VERSION}',
                             'uppReturnTarget=_top',
                             'mode=lightbox'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Origin: https://webshop-qa.arweb.pp.ciklum.com',
                                   'Referer: https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/',
                                   'Cache-Control: max-age=0'),
                        # verify_text('.'),
                        TT(0.01),
                        extract_from_body(r'name="datatransTrxId"\n\s+value="(\d+?)"', 'TRX_ID'),
     ## >>>  <!DOCTYPE html>\n<html>\n <head>\n \t<title>.</title>\n \t<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>\n    <meta http-equiv="Cache-Control" CONTENT="no-cache"/>\n    <meta http-equiv=...
     ],

    ["PLACE_ORDER_DT_jsp/upStart_1.jsp",
                        POST('https://pilot.datatrans.biz/upp/jsp/upStart_1.jsp'),
                        body('datatransTrxId=${TRX_ID}',
                             'hiddenFrame=false',
                             'uppScreenWidth=999',
                             'iframed=true'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Host: pilot.datatrans.biz',
                                   'Origin: https://pilot.datatrans.biz',
                                   'Referer: https://pilot.datatrans.biz/upp/jsp/upStart.jsp',
                                   'Cache-Control: max-age=0'),
                        verify_text('ARVI'),
                        TT(0.01),
                        ## >>>  <!DOCTYPE html>\n<html class="" dir="ltr" lang="en">\n<head>\n    <base href="https://pilot.datatrans.biz/upp/payment/DT2015/"/>\n    <title>Demo ARVI SA Payment Page</title>\n    <meta charset="utf-8"/>\n ...
     ],

    end_transaction,

    ["SELECT_VISA_DT_upp/payment",
                        POST('https://pilot.datatrans.biz/upp/payment?pmethod=VIS'),
                        body('datatransTrxId=${TRX_ID}',
                             'pmethod=VIS'),
                        add_header('Accept: */*',
                                   'X-Requested-With: XMLHttpRequest',
                                   'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'),
                        verify_text('Demo ARVI SA Payment Page'),
                        TT(5),
                        ## >>>  <!DOCTYPE html>\n<html class="" dir="ltr" lang="en">\n<head>\n    <base href="https://pilot.datatrans.biz/upp/payment/DT2015/"/>\n    <title>Demo ARVI SA Payment Page</title>\n    <meta charset="utf-8"/>\n ...
     ],

    transaction_controller('Input Details And Pay (DT)'),

    ["INPUT_DETAILS_AND_PAY_DT_payment/ajax",
                        POST('https://pilot.datatrans.biz/upp/payment/ajax'),
                        body('datatransTrxId=${TRX_ID}',
                             'pmethod=VIS',
                             'expy=18',
                             'expm=12',
                             'cardno=4900+0000+0000+0086',
                             'expiry=12+%2F+18',
                             'cvv=123'),
                        add_header('Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
                                   'X-URL: /jsp/upStoreTrx.jsp',
                                   'Accept: */*',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('waiting'),
                        TT(5),
                        ## >>>  {"result":"waiting"}
     ],

    ["INPUT_DETAILS_AND_PAY_DT_jsp/upPayTrx_submit.jsp",
                        GET('https://pilot.datatrans.biz/upp/jsp/upPayTrx_submit.jsp?',
                            'datatransTrxId=${TRX_ID}',
                            '_=${__time(,)}'),
                        add_header('Accept: */*',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('3D_submit'),
                        TT(0.01),
                        ## >>>  3D_submit_1u.jsp
     ],

    ["INPUT_DETAILS_AND_PAY_DT_jsp/3D_submit_1u.jsp",
                        GET('https://pilot.datatrans.biz/upp/jsp/3D_submit_1u.jsp?datatransTrxId=${TRX_ID}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('3D submit'),
                        TT(0.01),
                        extract_from_body(r'name="PaReq" value="(.+?)"', 'PA_REQ'),
                        extract_from_body(r'name="TermUrl" value="(.+?)"', 'TERM_URL'),
                        python_assertion('''
origin_req = @get_var('PA_REQ')
origin_url = @get_var('TERM_URL')
new_req = @decode_string(origin_req)
new_url = @decode_string(origin_url)
@set_var('PA_REQ', new_req)
@set_var('TERM_URL', new_url)
'''),
                        ## >>>  <!DOCTYPE html>\n<html>\n    <head>\n        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>\n        <title>3D submit</title>\n\t</head>\n\n<body onLoad="document.pareqForm.submit()">\n\t<...
     ],

    ["INPUT_DETAILS_AND_PAY_DT_jsp/3d/banka_0u.jsp",
                        POST('https://pilot.datatrans.biz/upp/jsp/3d/banka_0u.jsp'),
                        body('MD=${TRX_ID}',
                             'PaReq=${PA_REQ}',
                             'TermUrl=${TERM_URL}',
                             'cardType=1',
                             'testOnly=yes'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Verified by VISA'),
                        TT(0.01),
                        extract_from_body(r'NAME="password_3d" VALUE="(.+?)"', 'PASSWORD_3D'),
                        ## >>>  <html>\n<head>\n\n<title>Verified by VISA</title>\n<style>\n.text {font-family: Arial, tahoma, sans-serif; font-size: 11px;\n\t\tcolor:#083884;}\n</style>\n\n\n  <SCRIPT language="JavaScript1.2">\n\n   function can...
     ],

    end_transaction,

    transaction_controller('Submit Payment (DT -> WS)'),

    ["SUBMIT_PAYMENT_DT_jsp/3d/banka_1u.jsp",
                        POST('https://pilot.datatrans.biz/upp/jsp/3d/banka_1u.jsp'),
                        body('MD=${TRX_ID}',
                             'PaReq=${PA_REQ}',
                             'error=0',
                             'cardType=1',
                             'password_3d=${PASSWORD_3D}',
                             'x=${__Random(1, 50)}',  # 39
                             'y=${__Random(1, 50)}'),  # 5
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('3Dsecure'),
                        TT(5),
                        extract_from_body(r'NAME="PaRes" VALUE="(.+?)"', 'PA_RES'),
                        python_assertion('''
origin_res = @get_var('PA_RES')
new_res = @decode_string(origin_res)
@set_var('PA_RES', new_res)
''')
                        ## >>>  <html>\n\n <head>\n      <title>3Dsecure</title>\n      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n\n\n  <SCRIPT language="JavaScript1.2">\n\n   function mySubmit()\n   {\n\tdocument.ban...
     ],

    ["SUBMIT_PAYMENT_DT_jsp/3D_wbserver_1u.jsp",
                        POST('https://pilot.datatrans.biz/upp/jsp/3D_wbserver_1u.jsp'),
                        body('MD=${TRX_ID}',
                             'PaRes=${PA_RES}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Demo ARVI'),
                        TT(0.01),
                        ## >>>  <!DOCTYPE html>\n<html class="" dir="ltr" lang="en">\n<head>\n    <base href="https://pilot.datatrans.biz/upp/payment/DT2015/"/>\n    <title>Demo ARVI SA Payment Page</title>\n    <meta charset="utf-8"/>\n ...
     ],

    ["SUBMIT_PAYMENT_DT_jsp/upPayment.jsp",
                        GET('https://pilot.datatrans.biz/upp/jsp/upPayment.jsp?',
                            'datatransTrxId=${TRX_ID}',
                            '_=${__time(,)}'),
                        add_header('Accept: */*',
                                   'X-Requested-With: XMLHttpRequest'),
                        verify_text('upReturn'),
                        TT(0.01),
                        ## >>>  upReturn.jsp
     ],

    ["SUBMIT_PAYMENT_DT_jsp/upReturn.jsp",
                        GET('https://pilot.datatrans.biz/upp/jsp/upReturn.jsp?datatransTrxId=${TRX_ID}'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                                   'Host: pilot.datatrans.biz',
                                   'Referer: https://pilot.datatrans.biz/upp/jsp/3D_wbserver_1u.jsp',
                                   'Accept-Encoding: gzip, deflate, br',
                                   'Accept-Language: en-US,en;q=0.9,uk;q=0.8,ru;q=0.7'),
                        verify_text('success'),
                        TT(0.01),
                        extract_from_body(r'cstrxid&#x3d;(\d+?)"', 'CSTRX_ID'),
                        extract_from_body(r'name="aliasCC" value="(.+?)"', 'ALIAS_CC'),
                        extract_from_body(r'name="reqtype" value="(.+?)"', 'REQ_TYPE'),
                        extract_from_body(r'name="acqAuthorizationCode" value="(\d+?)"', 'ACQ_AUTH_CODE'),
                        extract_from_body(r'name="authorizationCode" value="(\d+?)"', 'AUTH_CODE')
                        ## >>>  <html>\n <head>\n\t<title>UPP</title>\n\t<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">\n\t<META HTTP-EQUIV="Pragma" CONTENT="no-cache">\n\t<META HTTP-EQUIV="Expires" CONTENT="0">\n</head>\n<body onLoad="d...
     ],

    ["PAYMENT_SUCCESS_process/success/",
                        POST('https://webshop-qa.arweb.pp.ciklum.com/DatatransCw/process/success/?',
                             'utm_nooverride=1',
                             'cstrxid=${CSTRX_ID}'),
                        body('uppCustomerEmail=${LOGIN}',
                             'useAlias=yes',
                             'uppModuleVersion=${MODULE_VERSION}',
                             'testOnly=yes',
                             'amount=${AMOUNT}',
                             'maskedCC=490000xxxxxx0086',
                             'aliasCC=${ALIAS_CC}',
                             'pmethod=VIS',
                             'mode=lightbox',
                             'uppCustomerFirstName=${FIRST_NAME}',
                             'sign=${SIGN}',
                             'uppCustomerName=${FIRST_NAME}+${LAST_NAME}',
                             'uppCustomerCountry=${COUNTRY_ID}',
                             'uppCustomerCity=${CITY}',
                             'uppCustomerZipCode=${POSTCODE}',
                             'refno=${REFNO}',
                             'uppCustomerDetails=yes',
                             'language=en',
                             'returnCustomerCountry=USA',
                             'reqtype=${REQ_TYPE}',
                             'acqAuthorizationCode=ACQ_AUTH_CODE',
                             'uppCustomerStreet=${STREET}',
                             'theme=DT2015',
                             'uppReturnTarget=_top',
                             'responseMessage=Authorized',
                             'uppTransactionId=${TRX_ID}',
                             'uppReturnMaskedCC=yes',
                             'responseCode=01',
                             'expy=18',
                             'merchantId=${MERCHANT_ID}',
                             'cwDataTransId=${TRANS_ID}',
                             'currency=${CURRENCY}',
                             'expm=12',
                             'uppCustomerLastName=${LAST_NAME}',
                             'version=${VERSION}',
                             'uppModuleName=Customweb+Magento',
                             'authorizationCode=${AUTH_CODE}',
                             'status=success',
                             'uppMsgType=web'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Content-Type: application/x-www-form-urlencoded',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('ARVI'),
                        TT(0.01),
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/checkout/onepage/success/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],

    '@common.SET_LOCATION',

    end_transaction,

    ["LOGOUT_account/logout/",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/logout/'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('ARVI'),
                        TT(5),
                        ## >>> @redirect to https://webshop-qa.arweb.pp.ciklum.com/arvi_customer/account/logoutSuccess/
                        ## >>> <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]-->\n<!--[if IE 9 ]>    <html lang="en" id="top" class="no-j...
     ],

    '@common.SET_LOCATION',

    ["LOGOUT_webshop-qa.arweb.pp.ciklum.com",
                        GET('https://webshop-qa.arweb.pp.ciklum.com/'),
                        add_header('Upgrade-Insecure-Requests: 1',
                                   'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),
                        verify_text('Home page'),
                        TT(5),
                        ## >>>  <!DOCTYPE html>\n\n<!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]-->\n<!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]-->\n<!--[if IE 8 ]>    <html lang="...
     ],

    '@common.SET_LOCATION',

]

if __name__ == '__main__':
    DSLConverter.compile_DSL_to_Jmeter_test_plan(mode='single_script')
